<?php

/*
* Plugin Name: Simple Content Plus
* Description: Simple plugin to add sidebar to single post
* Version: 4.0
* Author: Kamal
* Licence: GPL+2
* Text Domain: scp
*/

if(!defined('ABSPATH'))
	die();

add_action('wp_enqueue_scripts', 'scp_custom_css');

function scp_custom_css()
{
	wp_enqueue_style('scp-custom-stylesheet', plugin_dir_url(__FILE__).'css/scp-sidebar-style.css');
}

add_action('widgets_init', 'scp_register_sidebar');

function scp_register_sidebar(){

	register_sidebar(
		array(
			'name'	=>	'Post content Plus',
			'id'	=>	'scp-sidebar',
			'description'	=>	'Widget in this area displayed in single post',
			'before_widget'	=>	'<div class="widget scp-sidebar">',
			'after_widget'	=>	'</div>',
			'before_title'	=>	'<h2 class="widgettitle scp-sidebar-title">',
			'after_title'	=>	'</h2>',
		)
	);
}

add_filter('the_content', 'scp_add_sidebar_to_content');

function scp_add_sidebar_to_content($content){
	if( is_single() && is_active_sidebar('scp-sidebar') && is_main_query())
		dynamic_sidebar('scp-sidebar');
	return $content;
}

// Load the auto-update class
/*add_action( 'init', 'activate_au' );
function activate_au()
{
	require_once ( 'wp_autoupdate.php' );
	$plugin_current_version = '4.0';
	$plugin_remote_path = 'http://localhost:88/update.php';
	$plugin_slug = plugin_basename( __FILE__ );
	$license_user = 'user';
	$license_key = 'abcd';
	new WP_AutoUpdate ( $plugin_current_version, $plugin_remote_path, $plugin_slug, $license_user, $license_key );
}
*/
